// global websocket message
var socket;

// when Alpine is ready open websocket connection
document.addEventListener('alpine:init', () => {


    // init empty store
    Alpine.store('teams', [])
    Alpine.store('players', [])
    Alpine.store('leaderboards', [])

    // websocket
    socket = new WebSocket('ws://' + window.location.host);

    socket.onopen = (evt) => {
        // ask for teamnames from the server
        socket.send(JSON.stringify(
            {
                'type': 'getkickerdata'
            }
        ));
    };

    socket.onmessage = (evt) => {
        let msg = JSON.parse(evt.data)
        switch (msg.type) {
            // got teamnames => insert them into the lists
            case "kickerdata":
                teams = msg.kicker_data.teams
                players = msg.kicker_data.players
                lb = msg.kicker_data.leaderboards

                // store teamList in global store
                Alpine.store('teams', teams);
                Alpine.store('players', players.sort(compareForAlphabeticSearch));
                Alpine.store('leaderboards', lb);

                break;

            // game has been started => reload to see game view
            case "startedgame":
                window.location.reload(true)
                break;
        }
    };
})

// function to handle entire part of adding new teams to the list
const addNewTeams = () => {
    return {
        newTeams: [],
        newTeamName: "",
        newTeamPlayers: [],
        playerSearch: "",
        availablePlayers: [],
        alert: {
            show: false,
            type: "alert-success",
            msg: ""
        },

        init() {
            this.$watch('$store.players', (players) => {
                this.availablePlayers = players.sort(compareForAlphabeticSearch);
            })
            // reset alert when user starts typing a new name
            this.$watch('newTeamName', (name) => {
                if (this.alert.show && name != "") {
                    this.alert.show = false
                }
            });
            this.$watch('playerSearch', (searchString) => {
                // get all player names that start with the search string and are not already selected
                this.availablePlayers = Alpine.store('players').filter(player => {
                    // case insensitive
                    return player.toLowerCase().startsWith(searchString.toLowerCase()) && this.newTeamPlayers.indexOf(player) == -1;
                })
            })
        },

        addNewTeamToArray() {
            if (!this.newTeamName){
                return;
            }
            const inWaitingList = this.newTeams.some(team => team.name === this.newTeamName);
            const inGlobalList = Alpine.store('teams').some(team => team.name === this.newTeamName);

            // if newTeamName is not already in waiting list and not in global list
            if (!inWaitingList && !inGlobalList) {
                const newTeam = {
                    name: this.newTeamName,
                    players: this.newTeamPlayers,
                }
                this.newTeams.push(newTeam);
            } else {
                // show corresponging alert
                if (inGlobalList) {
                    this.alert = {
                        show: true,
                        type: "alert-danger",
                        msg: "Dieses Team existiert bereits."
                    }
                }
                if (inWaitingList) {
                    this.alert = {
                        show: true,
                        type: "alert-danger",
                        msg: "Dieses Team befindet sich bereits in der Warteliste für zu erstellende Teams."
                    }
                }
            }

            // empty entry field
            this.newTeamName = "";
            this.newTeamPlayers = [];
        },
        removeTeamFromList(index) {
            this.newTeams.splice(index, 1)
        },
        sendNewTeamsToServer() {
            const msg = {
                'type': 'newteams',
                'teams': this.newTeams
            }
            socket.send(JSON.stringify(msg));

            // empty alls entry fields
            this.newTeamName = "";
            this.newTeamPlayers = [];
            this.newTeams = [];

            // alert user
            this.alert.show = true;
            this.alert.msg = "Teams an den Server gesendet.";
        },
        addPlayerToTeam(name) {
            // only add player if not already in list 
            if(this.newTeamPlayers.indexOf(name) == -1){
                this.newTeamPlayers.push(name);
            }
            // empty search field
            this.playerSearch = "";
            // update availablePlayers list
            this.availablePlayers = this.availablePlayers.filter(val => {
                return val != name
            })

        },
        // on enter in search field: if only one name in availablePlayers add this player
        handleEnterAddPlayer() {
            if (this.availablePlayers.length == 1){
               this.addPlayerToTeam(this.availablePlayers[0]);
            }
        },
        removePlayerFromTeam(index) {
            var playerName = this.newTeamPlayers[index]
            this.newTeamPlayers.splice(index, 1); 
            this.availablePlayers.push(playerName)
            this.availablePlayers.sort(compareForAlphabeticSearch)
        }
    }
}

// function to handle entire part of adding new leatherboard
const addNewLeaderboard = () => {
    return {
        name: "",
        type: "open",
        info: "",
        teams: [],
        alert: {
            show: false,
            type: "alert-success",
            msg: ""
        },
        sendToServer() {
            const msg = {
                type: 'newleaderboard',
                name: this.name,
                ldb_type: this.type,
                info: this.info,
                teams: this.teams,
            }
            socket.send(JSON.stringify(msg));

            //reset box
            this.name = "";
            this.type = "open";
            this.info = "";
        },
    }
}

const addNewPlayers = () => {
    return {
        newPlayers: [],
        newPlayerName: "",
        alert: {
            show: false,
            type: "alert-success",
            msg: ""
        },

        init() {
            // reset alert when user starts typing a new name
            this.$watch('newPlayerName', (name) => {
                if (this.alert.show && name != "") {
                    this.alert.show = false
                }
            });
        },

        addNewPlayerToArray() {
            const inWaitingList = this.newPlayers.some(player => player.name === this.newPlayerName);
            const inGlobalList = Alpine.store('players').indexOf(this.newPlayerName) != -1;

            // if newPlayerName is not already in waiting list and not in global list
            if (!inWaitingList && !inGlobalList) {
                const newPlayer = {
                    name: this.newPlayerName,
                }
                this.newPlayers.push(newPlayer);
            } else {
                // show corresponging alert
                if (inGlobalList) {
                    this.alert = {
                        show: true,
                        type: "alert-danger",
                        msg: "Diese:r Spieler:in existiert bereits."
                    }
                }
                if (inWaitingList) {
                    this.alert = {
                        show: true,
                        type: "alert-danger",
                        msg: "Diese:r Spieler:in befindet sich bereits in der Warteliste für zu erstellende Teams."
                    }
                }
            }

            // empty entry field
            this.newPlayerName = ""
        },
        removePlayerFromList(index) {
            this.newPlayers.splice(index, 1)
        },
        sendNewPlayersToServer() {
            const msg = {
                'type': 'newplayers',
                'players': this.newPlayers
            }
            socket.send(JSON.stringify(msg));

            this.newPlayerName = "";
            this.newPlayers = [];

            // alert user
            // todo: do proper error handling => only show success msg when msg actually gone through
            this.alert.type = "alert-success";
            this.alert.msg = "Die neuen Spieler:innen wurden erfolgreich an den Server gesendet.";
            this.alert.show = true;
        },
    }
}
// function to handle entire part of selecting teams and starting the game
const teamNames = () => {
    return {
        // team name that is currently selected as team a
        selectedA: "",
        // team name that is currently selected as team b
        selectedB: "",
        // leaderboard currently selected
        selectedLb: "",

        isValid: false,

        alert: {
            show: false,
            type: "alert-success",
            msg: ""
        },

        init() {
            // watch team list in global store to update element accordingly
            this.$watch('$store.teams', (teams) => {
                if (teams.length >= 2) {
                    this.alert.show = false;
                    this.isValid = true;
                    this.selectedA = teams[0].name;
                    this.selectedB = teams[1].name;
                } else {
                    this.alert.show = true;
                    this.isValid = false;
                    this.alert.type = "alert-warning";
                    this.alert.msg = "Not enough teams yet. Please create new ones.";
                }
            }
            )

            // watch leaderboard list in global store to update element accordingly
            this.$watch('$store.leaderboards', (lb) => {
                if (lb.length > 0) {
                    this.alert.show = false;
                    this.isValid = true;
                    this.selectedLb = lb[0].name
                } else {
                    this.alert.show = true;
                    this.isValid = false;
                    this.alert.type = "alert-warning";
                    this.alert.msg = "Es gibt noch keine Leaderboards. Bitte erstelle eins.";
                }
            }
            )
        },

        // function to start the game
        startGame() {
            socket.send(JSON.stringify(
                {
                    'type': 'startgame',
                    'team_red': this.selectedA,
                    'team_blue': this.selectedB,
                    'leaderboard': this.selectedLb,
                }
            ))

        }
    }
}


// helper functions
const compareForAlphabeticSearch = (a,b) => {
     if (a < b){
            return -1
        }
        if (a > b){
            return 1
        }
        return 0

}
