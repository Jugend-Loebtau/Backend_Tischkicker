from channels.layers import get_channel_layer
from . import scoreDisplay
from .models import Game, Team, Leaderboard
try:
    import RPi.GPIO as GPIO
except ImportError:
    # mock GPIO for development
    import Mock.GPIO as GPIO

tor_rot = 33
tor_blau = 12
GPIO.setup(tor_rot, GPIO.IN)
GPIO.setup(tor_blau, GPIO.IN)
GPIO.setmode(GPIO.BOARD)

def listen_goal():
    GPIO.add_event_detect(tor_rot, GPIO.RISING, callback=lambda: manage_goal(by_team='team_red'), bouncetime=50)
    GPIO.add_event_detect(tor_blau, GPIO.RISING, callback=lambda: manage_goal(by_team='team_blue'), bouncetime=50)

'''def manage_goal(by_team):
    # update the database
    g = Game.objects.get(running=True)

    if by_team == 'team_red':
        g.score_red += 1
    if by_team == 'team_blue':
        g.score_blue += 1

    g.save()

    # update the sevensegment
    scoreDisplay.displayScore7Segment(score=g.score_red, team=0)
    scoreDisplay.displayScore7Segment(score=g.score_blue, team=1)

    # check if a team has won
    if g.score_blue >= 10 or g.score_red >= 10:
        g.running = False
        g.save()

    # make LEDs blink
    #     ======   ======   ======  ========\\    //\\    //\\    // ||
    #    ||    || ||    || ||    ||    ||    \\  //  \\  //  \\  //  ||
    #    ||====|| ||====|| ||====||    ||     \\//    \\//    \\//   ||
    #    ||       ||    || || \\       ||      ||      ||      ||    ||
    #    ||       ||    || ||  \\      ||      ||      ||      ||    ()


    # send new score to clients per websocket and

    channel_layer = get_channel_layer()

    channel_layer.group_send(
        "kickerclients",
        {
            "type": "scoreupdate",
            "score_red": g.score_red,
            "score_blue": g.score_blue,
        },
    )'''

