from django.urls import path

from . import views

app_name = 'kickerapp'

urlpatterns = [
    path('', views.home, name='home'),
    path('stats/', views.stats, name='statshome'),
    path('stats/<leaderboard>/', views.stats, name='stats')
]
